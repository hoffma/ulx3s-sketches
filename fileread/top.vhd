library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
    port 
    (
        clk_25mhz : in std_logic;
        btn : in std_logic_vector(1 downto 0);

        led : out std_logic_vector(7 downto 0);
        wifi_gpio0 : out std_logic
    );
end top;

architecture Behavioral of top is
    signal s_count : std_logic_vector(31 downto 0) := (others => '0');
    signal s_reset : std_logic;
    signal s_addr : std_logic_vector(2 downto 0) := (others => '0');
    signal s_dout : std_logic_vector(15 downto 0);
begin
    s_reset <= btn(1);
    led <= s_dout(7 downto 0);
    
    mem_inst : entity work.mem port map(clk => clk_25mhz,
                                    reset => s_reset,
                                    addr => s_addr,
                                    dout => s_dout);


    process(clk_25mhz)
    begin
        if rising_edge(clk_25mhz) then
            s_count <= std_logic_vector(unsigned(s_count) + 1);

            if s_count(27) = '1' then
                s_count <= (others => '0');
                s_addr <= std_logic_vector(unsigned(s_addr) + 1);
            end if;
            if s_reset = '1' then
                s_count <= (others => '0');
            end if;
        end if;
    end process;
end Behavioral;
