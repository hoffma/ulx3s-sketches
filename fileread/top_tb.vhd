library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is
end top_tb;

architecture Behavioral of top_tb is
    signal clk : std_logic := '0';
    signal s_reset : std_logic;
    signal s_addr : std_logic_vector(2 downto 0) := (others => '0');
    signal s_dout : std_logic_vector(15 downto 0);
begin
    dut : entity work.mem port map(clk => clk,
                                reset => s_reset,
                                addr => s_addr,
                                dout => s_dout);

    clk <= not clk after 5 ns;
    s_reset <= '1', '0' after 7 ns;

    process(clk)
    begin
        if rising_edge(clk) then
            s_addr <= std_logic_vector(unsigned(s_addr) + 1);

            if s_reset = '1' then
                s_addr <= (others => '0');
            end if;
        end if;
    end process;
end Behavioral;
