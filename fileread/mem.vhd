library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library std;
use std.textio.all;

entity mem is
    port 
    (
        clk : in std_logic;
        reset : in std_logic;
        addr : in std_logic_vector(2 downto 0);
        dout : out std_logic_vector(15 downto 0)
    );
end entity;

architecture Behavioral of mem is
    constant ram_depth : natural := 8;
    constant ram_width : natural := 16;

    type ram_type is array (0 to ram_depth - 1)
      of std_logic_vector(ram_width - 1 downto 0);

    impure function init_ram_hex return ram_type is
      file text_file : text open read_mode is "ram_content.txt";
      variable text_line : line;
      variable ram_content : ram_type;
      variable c : character;
      variable offset : integer;
      variable hex_val : std_logic_vector(3 downto 0);
    begin
      for i in 0 to ram_depth - 1 loop
        readline(text_file, text_line);

        offset := 0;

        while offset < ram_content(i)'high loop
          read(text_line, c);

          case c is
            when '0' => hex_val := "0000";
            when '1' => hex_val := "0001";
            when '2' => hex_val := "0010";
            when '3' => hex_val := "0011";
            when '4' => hex_val := "0100";
            when '5' => hex_val := "0101";
            when '6' => hex_val := "0110";
            when '7' => hex_val := "0111";
            when '8' => hex_val := "1000";
            when '9' => hex_val := "1001";
            when 'A' | 'a' => hex_val := "1010";
            when 'B' | 'b' => hex_val := "1011";
            when 'C' | 'c' => hex_val := "1100";
            when 'D' | 'd' => hex_val := "1101";
            when 'E' | 'e' => hex_val := "1110";
            when 'F' | 'f' => hex_val := "1111";

            when others =>
              hex_val := "XXXX";
              assert false report "Found non-hex character '" & c & "'";
          end case;

          ram_content(i)(ram_content(i)'high - offset
            downto ram_content(i)'high - offset - 3) := hex_val;
          offset := offset + 4;

        end loop;
      end loop;

      return ram_content;
    end function;

    signal ram_hex : ram_type := init_ram_hex;
begin
    procesS(clk)
    begin
        if rising_edge(clk) then
            dout <= ram_hex(to_integer(unsigned(addr(2 downto 0))));

            if reset = '1' then
                dout <= (others => '0');
            end if;
        end if;
    end process;

end Behavioral;
