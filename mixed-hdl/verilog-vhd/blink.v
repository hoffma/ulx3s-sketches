module blinky (
    input clk_25mhz,
    output [7:0] led,
    output wifi_gpio0
);

    wire clki;
    assign clki = clk_25mhz;
    assign wifi_gpio0 = 1;

    clkgen inst_clkgen(
        .clk(clki),
        .cnt(led)
    );
endmodule
