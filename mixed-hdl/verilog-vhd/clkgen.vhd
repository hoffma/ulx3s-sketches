library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clkgen is 
    port(
        clk : in std_logic;
        cnt : out std_logic_vector(7 downto 0)
    );
end entity;

architecture Behavioral of clkgen is
    signal counter : unsigned(28 downto 0) := (others => '0');
begin

    process(clk)
    begin
        if rising_edge(clk) then
            counter <= counter + 1;
        end if;
    end process;

    cnt <= std_logic_vector(counter(28 downto 21));

end architecture;
