module clkgen (
  input  wire       clk,
  output wire [7:0] cnt
);

    reg [28:0] counter = 0;
    always @(posedge clk) begin
        counter <= counter + 1;
    end

    assign cnt = counter[28:21];

endmodule
