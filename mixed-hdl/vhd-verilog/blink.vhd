library ieee;
use ieee.std_logic_1164.all;

entity blinky is
    port(
        clk_25mhz : in std_logic;
        led : out std_logic_vector(7 downto 0);

        wifi_gpio0 : out std_logic
    );
end entity;

architecture Behavioral of blinky is

    component clkgen
        port(
            clk : in std_logic;
            cnt : out std_logic_vector(7 downto 0)
        );
    end component;
begin
    wifi_gpio0 <= '1'; -- keep board from rebooting

    inst_clkgen : clkgen 
        port map (
            clk => clk_25mhz,
            cnt => led
        );

end Behavioral;
