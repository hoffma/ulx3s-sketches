# Mixed language synthesis

Simple example for mixed language synthesis.

- verilog-vhd

This folder has a verilog top level entity (blinky.v) and a VHDL sub module (clkgen.vhd).

- vhd-verilog

This folder has a VHDL top level entity (blinky.vhd) and a verilog sub module (clkgen.v).
Be aware that, in order for this to work, a component for this module has to be
described, to be able to instanciate it (because GHDL can't find it otherwise)!


Inspiration to get this done came from Mithro's nice "fomu" workshop: https://workshop.fomu.im/en/latest/mixed-hdl.html
