library ieee;
use ieee.std_logic_1164.all;

library std;
use std.env.finish;

entity snes_tb is
end snes_tb;

architecture behave of snes_tb is
    signal clk, rst : std_logic := '0';

    signal btn_in : std_logic_vector(15 downto 0) := x"f141"; -- B,left,A
    signal dat_o : std_logic_vector(11 downto 0) := (others => '0');
    signal dclk, dlatch, sdat : std_logic := '0';
    signal sdat_en : std_logic := '0';
    signal data_valid : std_logic := '0';
    signal sdat_i : std_logic;

    signal b, y, sel, start, bup, bdown, bleft, bright, a, x, l, r : std_logic;
begin
    clk <= not clk after 20 ns;
    rst <= '1', '0' after 90 ns;
    sdat <= sdat_i when sdat_en = '1' else '1';

    b <= dat_o(0);
    y <= dat_o(1);
    sel <= dat_o(2);
    start <= dat_o(3);
    bup <= dat_o(4);
    bdown <= dat_o(5);
    bleft <= dat_o(6);
    bright <= dat_o(7);
    a <= dat_o(8);
    x <= dat_o(9);
    l <= dat_o(10);
    r <= dat_o(11);

    stim: process
    begin
        sdat_i <= '0';
        sdat_en <= '0';
        wait until rst = '0';
        report "simulation started";
        --  wait until dlatch = '1';
        wait until dlatch = '0';
        report "got latch 0";
        sdat_en <= '1';
        for I in 0 to 11 loop
            sdat_i <= btn_in(I);
            report "I: " & integer'image(I);
            wait until rising_edge(dclk);
        end loop;
        -- wait until data_valid = '1';
        sdat_en <= '0';
        report "got data valid signal";
        report "waiting for next latch";
        wait until dlatch = '1';

        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        wait until rising_edge(clk);
        report "simulation finished";
        finish;
    end process;

    inst_snes: entity work.snes
    port map (
        clk => clk,
        rst => rst,
        btns => dat_o,
        dclk => dclk,
        dlatch => dlatch,
        sdat => sdat,
        data_valid => data_valid
    );
end behave;
