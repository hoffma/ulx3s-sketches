

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
    Port (
        clk_25mhz : in STD_LOGIC;
        btn : in STD_LOGIC_VECTOR(1 downto 0);
        gp1_sdat : in STD_LOGIC;
        gp2_dlatch : out STD_LOGIC;
        gp3_dclk : out STD_LOGIC;
        led : out STD_LOGIC_VECTOR(7 downto 0);
        wifi_gpio0 : out STD_LOGIC
    );
end top;
-- 3 clk
-- 2 latch
-- 1 data

architecture behave of top is
    signal clocks : std_logic_vector(3 downto 0);
    signal clk25 : std_logic;

    signal dclk, dlatch, sdat : std_logic;
    signal rst : std_logic;
    signal btns : std_logic_vector(11 downto 0);
    signal data_valid : std_logic;
begin
    wifi_gpio0 <= '1';
    -- gp(1) <= 'Z'; -- data is read port
    -- sdat <= gp(1);
    sdat <= gp1_sdat;
    gp2_dlatch <= dlatch;
    gp3_dclk <= dclk;
    rst <= btn(1);

    -- inst_clk : entity work.ecp5pll
    -- generic map (
    --     in_hz => 25000000,
    --     out0_hz => 25000000
    -- ) port map (
    --     clk_i => clk_25mhz,
    --     clk_o => clocks
    -- );
    clk25 <= clk_25mhz;

    process(clk25)
    begin
        if rising_edge(clk25) then
            if data_valid = '1' then
                led <= btns(4) & btns(5) & btns(6) & btns(7) & btns(1) & btns(9) & btns(0) & btns(8);
                -- led <= btns(7 downto 0);
            end if;
        end if;
    end process;
    -- 0 sel
    -- 1 start
    -- 2 up
    -- 3 down
    -- 4 left
    -- 5 right
    -- 6 a
    -- 7 x

    inst_snes: entity work.snes
    port map (
        clk => clk25,
        rst => rst,
        btns => btns,
        dclk => dclk,
        dlatch => dlatch,
        sdat => sdat,
        data_valid => data_valid
    );
end behave;
