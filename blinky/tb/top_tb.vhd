
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_tb is
end top_tb;

architecture behave of top_tb is

    signal clk : std_logic := '0';
    signal led : std_logic_vector(7 downto 0);
begin

    clk <= not clk after 25 ns;

    inst_top : entity work.top 
    generic map (
        LOWER_BIT => 1
    )
    port map (
        clk_25mhz => clk,
        led => led
    );


end behave;
