

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
    Generic (
        LOWER_BIT : integer := 24
    );
    Port (
        clk_25mhz : in STD_LOGIC;
        led : out STD_LOGIC_VECTOR(7 downto 0);
        wifi_gpio0 : out STD_LOGIC
    );
end top;

architecture behave of top is
    signal clocks : std_logic_vector(3 downto 0);
    signal clk250 : std_logic;
    signal cnt : unsigned(31 downto 0) := (others => '0');

begin

    wifi_gpio0 <= '1';

    inst_clk : entity work.ecp5pll
    generic map (
        in_hz => 25000000,
        out0_hz => 250000000
    ) port map (
        clk_i => clk_25mhz,
        clk_o => clocks
    );

    clk250 <= clocks(0);

    process(clk250)
    begin
        if rising_edge(clk250) then
            cnt <= cnt + 1;
        end if;
    end process;

    led <= std_logic_vector(cnt(LOWER_BIT+7 downto LOWER_BIT));

end behave;
