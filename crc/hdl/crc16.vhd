library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.ALL;

entity CRC16 is
    Port (
        clk         : in std_logic;
        rst         : in std_logic;
        data_in     : in std_logic_vector(7 downto 0);
        init_in     : in std_logic_vector(15 downto 0);
        crc_out     : out std_logic_vector(15 downto 0);
        start_calc  : in std_logic;
        done_calc   : out std_logic
    );
end CRC16;

architecture Behavioral of CRC16 is
    type state_t is (ST_WAIT, ST_CALC);
    signal curr_state : state_t;

    signal crc_reg: std_logic_vector(15 downto 0) := (others => '0');
    constant polynomial : std_logic_vector(15 downto 0) := x"1021";
begin
    process(clk)
        variable cycles : integer;
    begin
        if rising_edge(clk) then
            done_calc <= '0';
            case curr_state is
                when ST_WAIT =>
                    cycles := 7;
                    if start_calc = '1' then
                        curr_state <= ST_CALC;
                        crc_reg <= init_in;
                    end if;
                when ST_CALC =>
                    if crc_reg(15) /= data_in(cycles) then
                        crc_reg <= crc_reg(14 downto 0) & '0' XOR polynomial;
                    else
                        crc_reg <= crc_reg(14 downto 0) & '0';
                    end if;
                    if cycles = 0 then
                        done_calc <= '1';
                        curr_state <= ST_WAIT;
                    end if;
                    cycles := cycles - 1;
            end case;
            if rst = '1' then
                crc_reg <= init_in;
                curr_state <= ST_WAIT;
                done_calc <= '0';
            end if;
        end if;
    end process;
    crc_out <= crc_reg;
end Behavioral;
