

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
    Generic (
        LOWER_BIT : integer := 20
    );
    Port (
        clk_25mhz : in STD_LOGIC;
        led : out STD_LOGIC_VECTOR(7 downto 0);
        wifi_gpio0 : out STD_LOGIC
    );
end top;

architecture behave of top is

    signal cnt : unsigned(31 downto 0) := (others => '0');

begin

    wifi_gpio0 <= '1';

    process(clk_25mhz)
    begin
        if rising_edge(clk_25mhz) then
            cnt <= cnt + 1;
        end if;
    end process;

    led <= std_logic_vector(cnt(LOWER_BIT+7 downto LOWER_BIT));

end behave;
