library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;
use std.env.finish;

entity crc16_tb is
end crc16_tb;

architecture behave of crc16_tb is
    signal clk : std_logic := '0';
    signal rst : std_logic := '0';
    signal en : std_logic;
    signal ack : std_logic;
    signal din : std_logic_vector(7 downto 0);
    signal crc_out : std_logic_vector(15 downto 0);
    
    signal init : std_logic_vector(15 downto 0) := (others => '0');


    function char2slv (c: character) return std_logic_vector is
        variable ret : std_logic_vector(7 downto 0);
    begin
        ret := std_logic_vector(to_unsigned(character'pos(c), 8));
        return ret;
    end function char2slv;

    -- signal inp_string : string := "Hi!";
    -- constant EXPECTED_CRC : std_logic_vector(15 downto 0) := x"31fd";

    type test_case_t is record
        input : line;
        expected : std_logic_vector(15 downto 0);
    end record test_case_t;
    type test_arr_t is array(natural range <>) of test_case_t;
begin

    clk <= not clk after 5 ns;
    rst <= '1', '0' after 100 ns;

    stim: process
        variable tmp_crc : std_logic_vector(15 downto 0);
        variable c : character;
        variable len : integer;

        variable test_cases : test_arr_t(0 to 2) := (
            (new String'("Hi!"), x"31fd"),
            (new String'("Hello, World!"), x"4fd6"),
            (new String'("Hello, world!"), x"7ade")
        );
    begin
        en <= '0';
        din <= (others => '0');
        tmp_crc := (others => '0');
        wait until rst = '0';
        report "Simulation started" severity note;

        len := test_cases'length;
        report "test_cases'length: " & integer'image(len) severity note;
        for j in 0 to test_cases'length-1 loop
            en <= '0';
            din <= (others => '0');
            tmp_crc := (others => '0');

            for i in test_cases(j).input'range loop
            --     -- din <= std_logic_vector(get_char_val(inp_string, i));
                c := character(test_cases(j).input(i));
                din <= char2slv(c);
                init <= tmp_crc;
                en <= '1';
                wait until rising_edge(clk);
                en <= '0';
                wait until ack = '1';
                wait until rising_edge(clk);
                tmp_crc := crc_out;
                report "Input: " 
                        & character'image(c) 
                        & " (" & to_hstring(din) 
                        & "), tmp_crc: " 
                        & "0x" & to_hstring(crc_out) severity note;
            end loop;
            assert tmp_crc = test_cases(j).expected severity FAILURE;
            report "Test case done. Final crc: 0x" & to_hstring(crc_out) severity note;
            wait for 200 ns;
        end loop;

        report "CRC correct. Simulation finished." severity note;
        finish;
    end process;

    inst_crc: entity work.crc16
    port map (
        clk => clk,
        rst => rst,
        data_in => din,
        init_in => init,
        crc_out => crc_out,
        start_calc => en,
        done_calc => ack
    );
end behave;
