library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ws2812b_tb is
end ws2812b_tb;

architecture behave of ws2812b_tb is
    signal clk : std_logic := '0';
    signal i_clr : std_logic_vector(23 downto 0);
    signal o_pxl : std_logic_vector(15 downto 0);
    signal o_nxt_pxl : std_logic_vector(15 downto 0);
    signal o_dout : std_logic;
    signal o_done : std_Logic;

    type colors_t is array(natural range<>) of std_logic_vector(23 downto 0);
    constant COLOR_DATA : colors_t(0 to 15) := (
        x"000000", -- black
        x"aa0000", -- blue
        x"00aa00", -- green
        x"aaaa00", -- cyan
        x"0000aa", -- red
        x"aa00aa", -- magenta
        x"0055aa", -- brown
        x"aaaaaa", -- light gray
        x"555555", -- dark gray
        x"ff5555", -- light blue
        x"55ff55", -- light green
        x"ffff55", -- light cyan
        x"5555ff", -- light red
        x"ff55ff", -- light magenta
        x"55ffff", -- yellow
        x"ffffff"  -- white
    );
begin

    clk <= not clk after 5 ns; -- 100 MHz

    -- i_clr <= x"000005";
    -- i_clr <= COLOR_DATA(1) when o_pxl(0) = '1' else COLOR_DATA(4);
    i_clr <= COLOR_DATA(to_integer(unsigned(o_pxl(3 downto 0))));

    inst_ws : entity work.ws2812b
    generic map (
        STRIP_LEN => 32,
        CLK_T_NS => 10
    )
    port map (
        clk => clk,
        color => i_clr,
        pixel => o_pxl,
        next_pixel => o_nxt_pxl,
        done => o_done,
        dout => o_dout
    );
end behave;
