

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
    Generic (
        LOWER_BIT : integer := 22
    );
    Port (
        clk_25mhz : in STD_LOGIC;
        led : out STD_LOGIC_VECTOR(7 downto 0);
        gp : out std_logic_vector(1 downto 0);
        wifi_gpio0 : out STD_LOGIC
    );
end top;

architecture behave of top is

    signal s_led : std_logic_vector(7 downto 0);

    signal cnt : unsigned(31 downto 0) := (others => '0');

    -- generated clocks
    signal clocks : std_logic_vector(3 downto 0);

    -- ws2812b signals
    signal i_clr : std_logic_vector(23 downto 0) := (others => '0');
    signal o_pxl : std_logic_vector(15 downto 0);
    signal o_nxt_pxl : std_logic_vector(15 downto 0);
    signal o_dout : std_logic;
    signal o_done : std_logic;
    constant CLK_NS : natural := 10;
    constant STRIP_LEN : natural := 16;

    type colors_t is array(natural range<>) of std_logic_vector(23 downto 0);
    constant COLOR_DATA : colors_t(0 to 15) := (
        --G R B
        x"000000", -- black
        x"0000aa", -- blue
        x"aa0000", -- green
        x"aa00aa", -- cyan
        x"00aa00", -- red
        x"00aaaa", -- magenta
        x"55aa00", -- brown
        x"aaaaaa", -- light gray
        x"555555", -- dark gray
        x"5555ff", -- light blue
        x"ff5555", -- light green
        x"ff55ff", -- light cyan
        x"55ff55", -- light red
        x"55ffff", -- light magenta
        x"ffff55", -- yellow
        x"ffffff"  -- white
    );
begin

    wifi_gpio0 <= '1';
    led <= s_led;

    gp(0) <= o_dout;

    --         G R B
    i_clr <= x"000000" when o_pxl(2 downto 0) = "000" else
             x"110000" when o_pxl(2 downto 0) = "001" else
             x"001100" when o_pxl(2 downto 0) = "010" else
             x"111100" when o_pxl(2 downto 0) = "011" else
             x"000011" when o_pxl(2 downto 0) = "100" else
             x"110011" when o_pxl(2 downto 0) = "101" else
             x"001111" when o_pxl(2 downto 0) = "110" else
             x"111111" when o_pxl(2 downto 0) = "111" else
             (others => '0');

    -- i_clr <= COLOR_DATA(to_integer(unsigned(o_pxl(3 downto 0))));
    -- i_clr <= x"000011";

    clkgen_inst : entity work.ecp5pll
    generic map (
        in_Hz => natural(25.0e6),
        out0_Hz => natural(100.0e6), out0_tol_hz => 0
    ) port map (
        clk_i => clk_25mhz,
        clk_o => clocks
    );

    process(clk_25mhz)
    begin
        if rising_edge(clk_25mhz) then
            cnt <= cnt + 1;
        end if;
    end process;

    s_led <= std_logic_vector(cnt(LOWER_BIT+7 downto LOWER_BIT));

    inst_ws : entity work.ws2812b
    generic map (
        STRIP_LEN => STRIP_LEN,
        CLK_T_NS => CLK_NS
    )
    port map(
        clk => clocks(0),
        color => i_clr,
        pixel => o_pxl,
        next_pixel => o_nxt_pxl,
        done => o_done,
        dout => o_dout
    );

end behave;
